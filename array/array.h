/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



// In a header file, define ARRAY_<TYPE> for each required type. Then include
// array.h once for each definition. Typedefs and declarations for each <TYPE>
// will be substituted. See the macro sequence below for the list of available
// <TYPE>s.
// #define ARRAY_CHAR
// #define ARRAY_PTR
// #include "array.h"
// #include "array.h"
//
// In a source file, include the header above and then add the same sequence of
// directives, except include array.c instead of array.h.
// #include "header.h"
// #define ARRAY_CHAR
// #define ARRAY_PTR
// #include "array.c"
// #include "array.c"
//
// In a setup with no header.h, just put the declarations and definitions into
// your source file.
// #define ARRAY_CHAR
// #define ARRAY_PTR
// #include "array.h"
// #include "array.h"
// #define ARRAY_CHAR
// #define ARRAY_PTR
// #include "array.c"
// #include "array.c"
//
// To obtain declarations for a custom type (any except an array), do the
// following. Multiple CUSTOM<X> slots are available for multiple custom types.
// #define ARRAY_CUSTOM1
// #define TYPE struct my_struct_t
// #define TYPESHORT mystruct
// #include "array.h"
//
// This will expand into the definitions.
// #include "header.h"
// #define ARRAY_CUSTOM1
// #define TYPE struct my_struct_t
// #define TYPESHORT mystruct
// #include "array.c"
//
// For the purposes of relation checks (smaller-than, equal, greater-than) two
// items of a custom type are evaluated in the memcmp() fashion. To set up a
// custom method, define COMPARE to the name of a function or function-like
// macro which takes two TYPE values and returns a number <0, =0, >0,
// representable in int, if the first value is smaller than, equal to or greater
// than the second, respectively. As usual, place the COMPARE define just before
// the inclusion of "array.[hc]". Comparison macros are allowed to multi-eval.
// their arguments.

#if defined ARRAY_CHAR
#undef ARRAY_CHAR
#ifndef ARRAY___CHAR_INCLUDED_H
#define TYPE char
#define TYPESHORT char
#define ARRAY___CHAR_INCLUDED_H
#endif

#elif defined ARRAY_UCHAR
#undef ARRAY_UCHAR
#ifndef ARRAY___UCHAR_INCLUDED_H
#define TYPE unsigned char
#define TYPESHORT uchar
#define ARRAY___UCHAR_INCLUDED_H
#endif

#elif defined ARRAY_BOOL
#undef ARRAY_BOOL
#ifndef ARRAY___BOOL_INCLUDED_H
#define TYPE bool
#define TYPESHORT bool
#define COMPARE ARRAY___CMP_RELOP
#define ARRAY___BOOL_INCLUDED_H
#endif

#elif defined ARRAY_WCHAR
#undef ARRAY_WCHAR
#ifndef ARRAY___WCHAR_INCLUDED_H
#define TYPE wchar_t
#define TYPESHORT wchar
#define COMPARE ARRAY___CMP_RELOP
#define ARRAY___WCHAR_INCLUDED_H
#endif

#elif defined ARRAY_PTR
#undef ARRAY_PTR
#ifndef ARRAY___PTR_INCLUDED_H
#define TYPE void *
#define TYPESHORT ptr
#define COMPARE ARRAY___CMP_RELOP_PTR
#define ARRAY___PTR_INCLUDED_H
#endif

#elif defined ARRAY_SIZE
#undef ARRAY_SIZE
#ifndef ARRAY___SIZE_INCLUDED_H
#define TYPE size_t
#define TYPESHORT size
#define ARRAY___SIZE_INCLUDED_H
#endif

#elif defined ARRAY_CUSTOM1
#undef ARRAY_CUSTOM1
#ifndef TYPE
#error "array.h #included for ARRAY_CUSTOM1 without a TYPE definition!"
#endif
#ifndef TYPESHORT
#error "array.h #included for ARRAY_CUSTOM1 without a TYPESHORT definition!"
#endif
#ifndef ARRAY___CUSTOM1_INCLUDED_H
#define ARRAY___CUSTOM1_INCLUDED_H
#else
#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif
#endif

#elif defined ARRAY_CUSTOM2
#undef ARRAY_CUSTOM2
#ifndef TYPE
#error "array.h #included for ARRAY_CUSTOM2 without a TYPE definition!"
#endif
#ifndef TYPESHORT
#error "array.h #included for ARRAY_CUSTOM2 without a TYPESHORT definition!"
#endif
#ifndef ARRAY___CUSTOM2_INCLUDED_H
#define ARRAY___CUSTOM2_INCLUDED_H
#else
#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif
#endif

#elif defined ARRAY_CUSTOM3
#undef ARRAY_CUSTOM3
#ifndef TYPE
#error "array.h #included for ARRAY_CUSTOM3 without a TYPE definition!"
#endif
#ifndef TYPESHORT
#error "array.h #included for ARRAY_CUSTOM3 without a TYPESHORT definition!"
#endif
#ifndef ARRAY___CUSTOM3_INCLUDED_H
#define ARRAY___CUSTOM3_INCLUDED_H
#else
#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif
#endif

#endif



#ifdef TYPE

#ifndef ARRAY___COMMON_MACROS  // common for all TYPEs and shared with the .c
#define ARRAY___COMMON_MACROS

#define ARRAY___CAT(x, y) x##y
#define ARRAY___CAT_EXP(x, y) ARRAY___CAT(x, y)
#define array_TYPESHORT ARRAY___CAT_EXP(array_, TYPESHORT)
#define array_TYPESHORT_empty ARRAY___CAT_EXP(array_TYPESHORT, _empty)
#define meminvariants_array_TYPESHORT ARRAY___CAT_EXP(meminvariants_, array_TYPESHORT)
#define ensalloc_array_TYPESHORT ARRAY___CAT_EXP(ensalloc_, array_TYPESHORT)
#define growalloc_array_TYPESHORT ARRAY___CAT_EXP(growalloc_, array_TYPESHORT)
#define trimmem_array_TYPESHORT ARRAY___CAT_EXP(trimmem_, array_TYPESHORT)
#define find_array_TYPESHORT ARRAY___CAT_EXP(find_, array_TYPESHORT)
#define findlast_array_TYPESHORT ARRAY___CAT_EXP(findlast_, array_TYPESHORT)
#define compare_wrapper_array_TYPESHORT ARRAY___CAT_EXP(compare_wrapper_array_, array_TYPESHORT)
#define sort_array_TYPESHORT ARRAY___CAT_EXP(sort_array_, array_TYPESHORT)
#define findsorted_array_TYPESHORT ARRAY___CAT_EXP(findsorted_, array_TYPESHORT)
#define append_array_TYPESHORT ARRAY___CAT_EXP(append_, array_TYPESHORT)
#define copy_array_TYPESHORT ARRAY___CAT_EXP(copy_, array_TYPESHORT)
#define zeroterm_array_TYPESHORT ARRAY___CAT_EXP(zeroterm_, array_TYPESHORT)
#define remove_array_TYPESHORT ARRAY___CAT_EXP(remove_, array_TYPESHORT)
#define insert_array_TYPESHORT ARRAY___CAT_EXP(insert_, array_TYPESHORT)

// this is not an inline func to enable custom structs/unions
#define ARRAY___CMP_RELOP(a, b) ((a)<(b) ? -1 : (a)==(b) ? 0 : 1)
#define ARRAY___CMP_RELOP_PTR(a, b) ((unsigned char *)(a)<(unsigned char *)(b) ? -1 : (unsigned char *)(a)==(unsigned char *)(b) ? 0 : 1)
#endif



#include <stddef.h>
#include <stdbool.h>

// always:
// 1. !a <==> !size
// 2. sizeof(TYPE) * len <= size
typedef struct {
	TYPE *a;
	size_t len;  // nr of valid items
	size_t size;  // bytes allocated
} array_TYPESHORT;

// init. with this or (array_TYPESHORT){0} mandatory
extern const array_TYPESHORT array_TYPESHORT_empty;



// to free: free(.a), to reuse afterwards, init with array_*_empty
// to shorten: .len = newval
// to use array_char in printf: f("%.*s", (int)(arr.len & INT_MAX), arr.a)

// All functions except trimmem() apply the following memory mgmt strategy.
// 1. When allocating memory or realloc()ing to a greater size, get only as much
//    (as much more) as necessary. Leave it up to the user to ens/growalloc()
//    preliminarily at will.
// 2. Never deallocate memory or realloc to a smaller size. Leave it up to the
//    user to trimmem() or free() at will.

// make sure there's space for n items
bool ensalloc_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t n);
// make sure there's space for (array->len + n) items
bool growalloc_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t n);
// make sure only the necessary amount of memory is allocated
bool trimmem_array_TYPESHORT(array_TYPESHORT array[const static 1]);
// replace array contents with a copy of whatlen items from what
bool copy_array_TYPESHORT(array_TYPESHORT array[const restrict static 1], const TYPE src[restrict static 1], size_t n);
// append a copy of whatlen items from what
bool append_array_TYPESHORT(array_TYPESHORT array[const restrict static 1], const TYPE src[restrict static 1], size_t n);
// zero-terminate array, array.len is not increased
bool zeroterm_array_TYPESHORT(array_TYPESHORT array[const static 1]);
// linear search for what in array
TYPE *find_array_TYPESHORT(const array_TYPESHORT array[static 1], TYPE what);
// backward linear search for what in array
TYPE *findlast_array_TYPESHORT(const array_TYPESHORT array[static 1], TYPE what);
// sort array (unstable)
void sort_array_TYPESHORT(array_TYPESHORT array[const static 1]);
// binary search in a sorted array
TYPE *findsorted_array_TYPESHORT(const array_TYPESHORT array[static 1], TYPE what);
void remove_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t start, size_t n);
bool insert_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t start, size_t n);

#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif

#endif
