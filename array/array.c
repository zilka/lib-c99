/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#if defined ARRAY_CHAR
#undef ARRAY_CHAR
#ifndef ARRAY___CHAR_INCLUDED_C
#define TYPE char
#define TYPESHORT char
#define ARRAY___CHAR_INCLUDED_C
#endif

#elif defined ARRAY_UCHAR
#undef ARRAY_UCHAR
#ifndef ARRAY___UCHAR_INCLUDED_C
#define TYPE unsigned char
#define TYPESHORT uchar
#define ARRAY___UCHAR_INCLUDED_C
#endif

#elif defined ARRAY_BOOL
#undef ARRAY_BOOL
#ifndef ARRAY___BOOL_INCLUDED_C
#define TYPE bool
#define TYPESHORT bool
#define COMPARE ARRAY___CMP_RELOP
#define ARRAY___BOOL_INCLUDED_C
#endif

#elif defined ARRAY_WCHAR
#undef ARRAY_WCHAR
#ifndef ARRAY___WCHAR_INCLUDED_C
#define TYPE wchar_t
#define TYPESHORT wchar
#define COMPARE ARRAY___CMP_RELOP
#define ARRAY___WCHAR_INCLUDED_C
#endif

#elif defined ARRAY_PTR
#undef ARRAY_PTR
#ifndef ARRAY___PTR_INCLUDED_C
#define TYPE void *
#define TYPESHORT ptr
#define COMPARE ARRAY___CMP_RELOP_PTR
#define ARRAY___PTR_INCLUDED_C
#endif

#elif defined ARRAY_SIZE
#undef ARRAY_SIZE
#ifndef ARRAY___SIZE_INCLUDED_C
#define TYPE size_t
#define TYPESHORT size
#define ARRAY___SIZE_INCLUDED_C
#endif

#elif defined ARRAY_CUSTOM1
#undef ARRAY_CUSTOM1
#ifndef TYPE
#error "array.c #included for ARRAY_CUSTOM1 without a TYPE definition!"
#endif
#ifndef TYPESHORT
#error "array.c #included for ARRAY_CUSTOM1 without a TYPESHORT definition!"
#endif
#ifndef ARRAY___CUSTOM1_INCLUDED_C
#define ARRAY___CUSTOM1_INCLUDED_C
#else
#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif
#endif

#elif defined ARRAY_CUSTOM2
#undef ARRAY_CUSTOM2
#ifndef TYPE
#error "array.c #included for ARRAY_CUSTOM2 without a TYPE definition!"
#endif
#ifndef TYPESHORT
#error "array.c #included for ARRAY_CUSTOM2 without a TYPESHORT definition!"
#endif
#ifndef ARRAY___CUSTOM2_INCLUDED_C
#define ARRAY___CUSTOM2_INCLUDED_C
#else
#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif
#endif

#elif defined ARRAY_CUSTOM3
#undef ARRAY_CUSTOM3
#ifndef TYPE
#error "array.c #included for ARRAY_CUSTOM3 without a TYPE definition!"
#endif
#ifndef TYPESHORT
#error "array.c #included for ARRAY_CUSTOM3 without a TYPESHORT definition!"
#endif
#ifndef ARRAY___CUSTOM3_INCLUDED_C
#define ARRAY___CUSTOM3_INCLUDED_C
#else
#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif
#endif

#endif



#ifdef TYPE

#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include "funcs-compat.h"

const array_TYPESHORT array_TYPESHORT_empty;



#ifndef NDEBUG  // gcc: unused function
static bool meminvariants_array_TYPESHORT(const array_TYPESHORT array[static 1]) {
	if ( (bool)array->a == (bool)array->size && array->size <= PTRDIFF_MAX &&
	     SIZE_MAX/sizeof(TYPE) >= array->len &&
	     array->len*sizeof(TYPE) <= array->size
	   ) {
		return true;
	}
	return false;
}
#endif

// for char, compiles into the same code as a specialized, hand-made function
bool ensalloc_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t n) {
	assert(meminvariants_array_TYPESHORT(array));

	const size_t newsize = n * sizeof(TYPE);
	if (newsize/sizeof(TYPE) != n || newsize > PTRDIFF_MAX) {
		errno = EOVERFLOW;
		return false;
	}
	if (array->size < newsize) {  // also covers n=0
		assert(newsize);
		TYPE *const p = realloc(array->a, newsize);
		if (! p)
			return false;
		array->a = p;
		array->size = newsize;
	}
	return true;
}

bool growalloc_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t n) {
	assert(meminvariants_array_TYPESHORT(array));

	n += array->len;
	if (n < array->len) {
		errno = EOVERFLOW;
		return false;
	}
	return ensalloc_array_TYPESHORT(array, n);
}

// false <=> there is space to be freed, but the realloc failed
bool trimmem_array_TYPESHORT(array_TYPESHORT array[const static 1]) {
	assert(meminvariants_array_TYPESHORT(array));

	const size_t newsize = array->len * sizeof(TYPE);
	if (newsize < array->size) {
		TYPE *const p = realloc(array->a, newsize);
		if (newsize) {
			if (! p)
				return false;
			array->a = p;
		}
		else array->a = NULL;
		array->size = newsize;
	}
	return true;
}



// src and array->a must be disjunct
bool copy_array_TYPESHORT(array_TYPESHORT array[const restrict static 1], const TYPE src[restrict static 1], size_t n) {
	// string.h funcs (such as memcpy()) only accept valid ptrs, even if size=0
	assert(PTRDIFF_MAX/sizeof(TYPE) >= n);

	if (! ensalloc_array_TYPESHORT(array, n))
		return false;
	memcpy(array->a, src, n*sizeof(TYPE));
	array->len = n;
	return true;
}

// src and array->a must be disjunct
bool append_array_TYPESHORT(array_TYPESHORT array[const restrict static 1], const TYPE src[restrict static 1], size_t n) {
	assert(PTRDIFF_MAX/sizeof(TYPE) >= n);

	if (! growalloc_array_TYPESHORT(array, n))
		return false;
	memcpy(array->a+array->len, src, n*sizeof(TYPE));
	array->len += n;
	return true;
}

// the terminator isn't included in len and no other functions account for it
bool zeroterm_array_TYPESHORT(array_TYPESHORT array[const static 1]) {
	if (! growalloc_array_TYPESHORT(array, 1))
		return false;
	// for char (and certainly more) this compiles into the same code as
	// "array->a[array->len] = 0;"
	memset(array->a+array->len, 0, sizeof(TYPE));
	return true;
}



// faster than lfind() under some circumstances
TYPE *find_array_TYPESHORT(const array_TYPESHORT array[static 1], TYPE what) {
#ifdef COMPARE
	for (size_t i=0; i<array->len; ++i) {
		// "if (! ARRAY___CMP_RELOP(array->a[i], what))" compiles into what
		// looks like faster code than "if (array->a[i] == what)"
		// COMPARE() may be a macro with multi-eval
		if (! COMPARE(array->a[i], what))
			return array->a+i;
	}
	return NULL;
#else
	if (! array->len)
		return NULL;

	if (sizeof(TYPE) == 1) {  // compile-time!
		// for char, resulting code is as fast as
		// "return memchr(array->a, what, array->len)"
		unsigned char byte;
		memcpy(&byte, &what, 1);
		return memchr(array->a, byte, array->len);
	}

	// TODO: this segment can be made faster for large TYPEs
	unsigned char *next, *cur=(unsigned char *)array->a;
	size_t offs, cursize=array->len*sizeof(TYPE);
	while ( (next = memmem(cur, cursize, &what, sizeof(TYPE))) &&
	        (offs = (next-cur) % sizeof(TYPE))
	      ) {
		next += sizeof(TYPE) - offs;
		cursize -= next - cur;
		cur = next;
	}
	return (TYPE *)next;
#endif
}

// TODO: probably faster to start from the back
TYPE *findlast_array_TYPESHORT(const array_TYPESHORT array[static 1], TYPE what) {
	TYPE *next = find_array_TYPESHORT(array, what);
	if (! next)
		return NULL;

	array_TYPESHORT array2 = *array;
	do {
		++next;
		array2.len -= next - array2.a;
		array2.a = next;
	} while (( next = find_array_TYPESHORT(&array2, what) ));
	return array2.a - 1;
}



static inline int compare_wrapper_array_TYPESHORT(const void *i1, const void *i2) {
	assert(i1 && i2);

#ifdef COMPARE
	// COMPARE() may be a macro with multi-eval
	return COMPARE(*(const TYPE *)i1, *(const TYPE *)i2);
#else
	return memcmp(i1, i2, sizeof(TYPE));
#endif
}

// TODO: if the sorting algorithm were here, there'd be no need for callback
// stability not guaranteed
void sort_array_TYPESHORT(array_TYPESHORT array[const static 1]) {
	if (array->len)
		qsort(array->a, array->len, sizeof(TYPE), compare_wrapper_array_TYPESHORT);
}

// bsearch() can't handle macros and it "may reorder elements of the array
// between calls to the comparison function". Does it mean that ...?
TYPE *findsorted_array_TYPESHORT(const array_TYPESHORT array[static 1], TYPE what) {
	size_t min=1, max=array->len, next;
	assert(min > 0 && max < SIZE_MAX);  // can always go one step past min, max
	int relation;

	while (min <= max) {
		next = min-1 + (max-min) / 2;
		assert(next+1 >= min && next+1 <= max);
#ifdef COMPARE
		// COMPARE() may be a macro with multi-eval
		relation = COMPARE(what, array->a[next]);
#else
		relation = memcmp(&what, array->a+next, sizeof(TYPE));
#endif
		if (relation == 0)
			return array->a+next;
		if (relation < 0)
			max = next;
		else min = next + 2;
	}
	return NULL;
}



// Remove n items from array, starting from the start-th item. The portion of
// the array that follows the n-th item is shifted left by n positions.
void remove_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t start, size_t n) {
	assert(start && start <= array->len);
	assert(n && SIZE_MAX-start >= n && start+n <= array->len+1);

	--start;
	memmove(array->a+start, array->a+(start+n), (array->len-start-n)*sizeof(TYPE));
	array->len -= n;
}

// Insert n new, uninitialized items into array. The start-th item will be the
// first of the new items.
bool insert_array_TYPESHORT(array_TYPESHORT array[const static 1], size_t start, size_t n) {
	assert(start && start <= array->len+1 && n);

	if (! growalloc_array_TYPESHORT(array, n))
		return false;
	--start;
	memmove(array->a+(start+n), array->a+start, (array->len-start)*sizeof(TYPE));
	array->len += n;
	return true;
}

#undef TYPE
#undef TYPESHORT
#ifdef COMPARE
#undef COMPARE
#endif

#endif
