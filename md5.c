/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#include <string.h>
#include <assert.h>
#include "md5.h"

// multi evaluation, 'a' assignable
#define R1(b, c, d) (((b)&(c)) | (~(b)&(d)))
#define R2(b, c, d) (((b)&(d)) | (~(d)&(c)))
#define R3(b, c, d) ((b) ^ (c) ^ (d))
#define R4(b, c, d) ((c) ^ ((b)|~(d)))
#define XFM(a, b, rfunc, k, s, i) do { \
	                                  assert((s) > 0 && (s) < 32); \
	                                  (a) += (rfunc) + (k) + (i); \
	                                  /* it's faster to trim 'a' beforehand */ \
	                                  /* (when it's not 32b) */ \
	                                  (a) &= 0xffffffff; \
	                                  (a) = (a) << (s) | (a) >> (32-(s)); \
	                                  (a) += (b); \
                                  } while (0)



static void hash_buffer(md5_digest dgst[const static 1]) {
	md5_arith_t abcd[4];
	memcpy(abcd, dgst->hash, sizeof(abcd));

	// No need to type-cast *x. It's not larger than *hash and won't change the
	// type the calculations take place in. Same for the 32b constants.
	XFM(abcd[0], abcd[1], R1(abcd[1], abcd[2], abcd[3]), dgst->x[0], 7, 0xd76aa478);
	XFM(abcd[3], abcd[0], R1(abcd[0], abcd[1], abcd[2]), dgst->x[1], 12, 0xe8c7b756);
	XFM(abcd[2], abcd[3], R1(abcd[3], abcd[0], abcd[1]), dgst->x[2], 17, 0x242070db);
	XFM(abcd[1], abcd[2], R1(abcd[2], abcd[3], abcd[0]), dgst->x[3], 22, 0xc1bdceee);
	XFM(abcd[0], abcd[1], R1(abcd[1], abcd[2], abcd[3]), dgst->x[4], 7, 0xf57c0faf);
	XFM(abcd[3], abcd[0], R1(abcd[0], abcd[1], abcd[2]), dgst->x[5], 12, 0x4787c62a);
	XFM(abcd[2], abcd[3], R1(abcd[3], abcd[0], abcd[1]), dgst->x[6], 17, 0xa8304613);
	XFM(abcd[1], abcd[2], R1(abcd[2], abcd[3], abcd[0]), dgst->x[7], 22, 0xfd469501);
	XFM(abcd[0], abcd[1], R1(abcd[1], abcd[2], abcd[3]), dgst->x[8], 7, 0x698098d8);
	XFM(abcd[3], abcd[0], R1(abcd[0], abcd[1], abcd[2]), dgst->x[9], 12, 0x8b44f7af);
	XFM(abcd[2], abcd[3], R1(abcd[3], abcd[0], abcd[1]), dgst->x[10], 17, 0xffff5bb1);
	XFM(abcd[1], abcd[2], R1(abcd[2], abcd[3], abcd[0]), dgst->x[11], 22, 0x895cd7be);
	XFM(abcd[0], abcd[1], R1(abcd[1], abcd[2], abcd[3]), dgst->x[12], 7, 0x6b901122);
	XFM(abcd[3], abcd[0], R1(abcd[0], abcd[1], abcd[2]), dgst->x[13], 12, 0xfd987193);
	XFM(abcd[2], abcd[3], R1(abcd[3], abcd[0], abcd[1]), dgst->x[14], 17, 0xa679438e);
	XFM(abcd[1], abcd[2], R1(abcd[2], abcd[3], abcd[0]), dgst->x[15], 22, 0x49b40821);

	XFM(abcd[0], abcd[1], R2(abcd[1], abcd[2], abcd[3]), dgst->x[1], 5, 0xf61e2562);
	XFM(abcd[3], abcd[0], R2(abcd[0], abcd[1], abcd[2]), dgst->x[6], 9, 0xc040b340);
	XFM(abcd[2], abcd[3], R2(abcd[3], abcd[0], abcd[1]), dgst->x[11], 14, 0x265e5a51);
	XFM(abcd[1], abcd[2], R2(abcd[2], abcd[3], abcd[0]), dgst->x[0], 20, 0xe9b6c7aa);
	XFM(abcd[0], abcd[1], R2(abcd[1], abcd[2], abcd[3]), dgst->x[5], 5, 0xd62f105d);
	XFM(abcd[3], abcd[0], R2(abcd[0], abcd[1], abcd[2]), dgst->x[10], 9, 0x2441453);
	XFM(abcd[2], abcd[3], R2(abcd[3], abcd[0], abcd[1]), dgst->x[15], 14, 0xd8a1e681);
	XFM(abcd[1], abcd[2], R2(abcd[2], abcd[3], abcd[0]), dgst->x[4], 20, 0xe7d3fbc8);
	XFM(abcd[0], abcd[1], R2(abcd[1], abcd[2], abcd[3]), dgst->x[9], 5, 0x21e1cde6);
	XFM(abcd[3], abcd[0], R2(abcd[0], abcd[1], abcd[2]), dgst->x[14], 9, 0xc33707d6);
	XFM(abcd[2], abcd[3], R2(abcd[3], abcd[0], abcd[1]), dgst->x[3], 14, 0xf4d50d87);
	XFM(abcd[1], abcd[2], R2(abcd[2], abcd[3], abcd[0]), dgst->x[8], 20, 0x455a14ed);
	XFM(abcd[0], abcd[1], R2(abcd[1], abcd[2], abcd[3]), dgst->x[13], 5, 0xa9e3e905);
	XFM(abcd[3], abcd[0], R2(abcd[0], abcd[1], abcd[2]), dgst->x[2], 9, 0xfcefa3f8);
	XFM(abcd[2], abcd[3], R2(abcd[3], abcd[0], abcd[1]), dgst->x[7], 14, 0x676f02d9);
	XFM(abcd[1], abcd[2], R2(abcd[2], abcd[3], abcd[0]), dgst->x[12], 20, 0x8d2a4c8a);

	XFM(abcd[0], abcd[1], R3(abcd[1], abcd[2], abcd[3]), dgst->x[5], 4, 0xfffa3942);
	XFM(abcd[3], abcd[0], R3(abcd[0], abcd[1], abcd[2]), dgst->x[8], 11, 0x8771f681);
	XFM(abcd[2], abcd[3], R3(abcd[3], abcd[0], abcd[1]), dgst->x[11], 16, 0x6d9d6122);
	XFM(abcd[1], abcd[2], R3(abcd[2], abcd[3], abcd[0]), dgst->x[14], 23, 0xfde5380c);
	XFM(abcd[0], abcd[1], R3(abcd[1], abcd[2], abcd[3]), dgst->x[1], 4, 0xa4beea44);
	XFM(abcd[3], abcd[0], R3(abcd[0], abcd[1], abcd[2]), dgst->x[4], 11, 0x4bdecfa9);
	XFM(abcd[2], abcd[3], R3(abcd[3], abcd[0], abcd[1]), dgst->x[7], 16, 0xf6bb4b60);
	XFM(abcd[1], abcd[2], R3(abcd[2], abcd[3], abcd[0]), dgst->x[10], 23, 0xbebfbc70);
	XFM(abcd[0], abcd[1], R3(abcd[1], abcd[2], abcd[3]), dgst->x[13], 4, 0x289b7ec6);
	XFM(abcd[3], abcd[0], R3(abcd[0], abcd[1], abcd[2]), dgst->x[0], 11, 0xeaa127fa);
	XFM(abcd[2], abcd[3], R3(abcd[3], abcd[0], abcd[1]), dgst->x[3], 16, 0xd4ef3085);
	XFM(abcd[1], abcd[2], R3(abcd[2], abcd[3], abcd[0]), dgst->x[6], 23, 0x4881d05);
	XFM(abcd[0], abcd[1], R3(abcd[1], abcd[2], abcd[3]), dgst->x[9], 4, 0xd9d4d039);
	XFM(abcd[3], abcd[0], R3(abcd[0], abcd[1], abcd[2]), dgst->x[12], 11, 0xe6db99e5);
	XFM(abcd[2], abcd[3], R3(abcd[3], abcd[0], abcd[1]), dgst->x[15], 16, 0x1fa27cf8);
	XFM(abcd[1], abcd[2], R3(abcd[2], abcd[3], abcd[0]), dgst->x[2], 23, 0xc4ac5665);

	XFM(abcd[0], abcd[1], R4(abcd[1], abcd[2], abcd[3]), dgst->x[0], 6, 0xf4292244);
	XFM(abcd[3], abcd[0], R4(abcd[0], abcd[1], abcd[2]), dgst->x[7], 10, 0x432aff97);
	XFM(abcd[2], abcd[3], R4(abcd[3], abcd[0], abcd[1]), dgst->x[14], 15, 0xab9423a7);
	XFM(abcd[1], abcd[2], R4(abcd[2], abcd[3], abcd[0]), dgst->x[5], 21, 0xfc93a039);
	XFM(abcd[0], abcd[1], R4(abcd[1], abcd[2], abcd[3]), dgst->x[12], 6, 0x655b59c3);
	XFM(abcd[3], abcd[0], R4(abcd[0], abcd[1], abcd[2]), dgst->x[3], 10, 0x8f0ccc92);
	XFM(abcd[2], abcd[3], R4(abcd[3], abcd[0], abcd[1]), dgst->x[10], 15, 0xffeff47d);
	XFM(abcd[1], abcd[2], R4(abcd[2], abcd[3], abcd[0]), dgst->x[1], 21, 0x85845dd1);
	XFM(abcd[0], abcd[1], R4(abcd[1], abcd[2], abcd[3]), dgst->x[8], 6, 0x6fa87e4f);
	XFM(abcd[3], abcd[0], R4(abcd[0], abcd[1], abcd[2]), dgst->x[15], 10, 0xfe2ce6e0);
	XFM(abcd[2], abcd[3], R4(abcd[3], abcd[0], abcd[1]), dgst->x[6], 15, 0xa3014314);
	XFM(abcd[1], abcd[2], R4(abcd[2], abcd[3], abcd[0]), dgst->x[13], 21, 0x4e0811a1);
	XFM(abcd[0], abcd[1], R4(abcd[1], abcd[2], abcd[3]), dgst->x[4], 6, 0xf7537e82);
	XFM(abcd[3], abcd[0], R4(abcd[0], abcd[1], abcd[2]), dgst->x[11], 10, 0xbd3af235);
	XFM(abcd[2], abcd[3], R4(abcd[3], abcd[0], abcd[1]), dgst->x[2], 15, 0x2ad7d2bb);
	XFM(abcd[1], abcd[2], R4(abcd[2], abcd[3], abcd[0]), dgst->x[9], 21, 0xeb86d391);

	dgst->hash[0] += abcd[0];
	dgst->hash[1] += abcd[1];
	dgst->hash[2] += abcd[2];
	dgst->hash[3] += abcd[3];
}



inline static const uchar *slow_mempcpy_u32(uint32_t x[const restrict static 1], const uchar data[restrict static 1], uf8 n) {
	assert(n);
	for (uf8 i=0; i<n; ++i) {
		x[i] = *data++;  // starting a new x[] item
		x[i] += *data++ << 8;
		x[i] += *data++ << 16;
		x[i] += *data++ << 24;
	}
	return data;
}



void md5_init(md5_digest dgst[const static 1]) {
	// x[] needs no init.
	dgst->buffered = dgst->msgsize = 0;
	memcpy(dgst->hash, (const md5_arith_t[4]){0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476}, sizeof(dgst->hash));
}



void md5_insert(md5_digest dgst[const restrict static 1], const uchar *restrict data, size_t size) {
	assert(! size || data);
	assert(dgst->buffered == dgst->msgsize % 64);

	// When hashing files 'buffered' is 0 most of the time and doesn't change.
	// Keep it in dgst, don't recompute here every time. Current x[] index isn't
	// needed usually - not recorded.
	dgst->msgsize += size;

	if (LE) {
		if (dgst->buffered) {
			const uf8 avail = 64 - dgst->buffered;
			if (size >= avail) {
				memcpy((uchar *)dgst->x+dgst->buffered, data, avail);
				hash_buffer(dgst);
				data += avail;
				size -= avail;
				dgst->buffered = 0;
			}
		}

		// most time is spent here when hashing files
		for (; size>=64; size-=64) {
			assert(! dgst->buffered);
			memcpy(dgst->x, data, 64);
			data += 64;
			hash_buffer(dgst);
		}

		if (size) {
			assert(dgst->buffered < 63 && size < 64 && size+dgst->buffered < 64);
			memcpy((uchar *)dgst->x+dgst->buffered, data, size);
			dgst->buffered += size;
		}
	}
	else {
		if (dgst->buffered) {
			for (; dgst->buffered%4 && size; ++dgst->buffered,--size)
				dgst->x[dgst->buffered/4] += *data++ << dgst->buffered%4*8;
			if (! (dgst->buffered % 4)) {
				// buffered can be 64, size can be 0
				uf8 avail = 64 - dgst->buffered;
				if (size >= avail) {
					if (avail) {
						data = slow_mempcpy_u32(dgst->x+dgst->buffered/4, data, avail/4);
						size -= avail;
					}
					hash_buffer(dgst);
					dgst->buffered = 0;
				}
			}
		}

		// most time is spent here when hashing files
		for (; size>=64; size-=64) {
			assert(! dgst->buffered);
			data = slow_mempcpy_u32(dgst->x, data, 16);
			hash_buffer(dgst);
		}

		if (size >= 4) {
			assert(size < 64);
			assert(dgst->buffered <= 56 && ! (dgst->buffered % 4));
			assert(dgst->buffered + size < 64);
			data = slow_mempcpy_u32(dgst->x+dgst->buffered/4, data, size/4);
			dgst->buffered += size & 0xfc;
			size %= 4;
		}

		if (size) {
			assert(size < 4);
			assert(dgst->buffered <= 60 && ! (dgst->buffered % 4));
			const uf8 curx = dgst->buffered / 4;
			dgst->x[curx] = 0;  // starting a new x[] item
			// it's faster to set to zero first and then accumulate
			for (; size; ++dgst->buffered,--size)
				dgst->x[curx] += *data++ << dgst->buffered%4*8;
		}
	}
}



void md5_final(md5_digest dgst[const static 1]) {
	const md5_msgsize_t msgsize_actual = dgst->msgsize;
	uf8 padsize = 64+56 - dgst->buffered;
	if (padsize > 64)
		padsize -= 64;
	md5_insert(dgst, (const uchar[64]){0x80}, padsize);
	assert(dgst->buffered == 56);
	// bytes -> bits
	dgst->x[14] = msgsize_actual << 3;
	dgst->x[15] = msgsize_actual >> 29;
	hash_buffer(dgst);
}



bool md5_equal(const md5_arith_t hash[static 4], const md5_arith_t hash2[static 4]) {
	if (sizeof(*hash) == 4)
		return ! memcmp(hash, hash2, 16);
	else return (hash[0] & 0xffffffff) == (hash2[0] & 0xffffffff) &&
	            (hash[1] & 0xffffffff) == (hash2[1] & 0xffffffff) &&
	            (hash[2] & 0xffffffff) == (hash2[2] & 0xffffffff) &&
	            (hash[3] & 0xffffffff) == (hash2[3] & 0xffffffff);
}

char *md5_hash2str(char md5str[const restrict static 32], const md5_arith_t hash[restrict static 4], bool capitals) {
	const char *const alpha = capitals ? "0123456789ABCDEF" : "0123456789abcdef";

	for (uf8 hashidx=0; hashidx<4; ++hashidx) {
		for (uf8 md5idx=0; md5idx<8; md5idx+=2) {
			md5str[hashidx*8+md5idx] = alpha[hash[hashidx]>>4*(md5idx+1)&0xf];
			md5str[hashidx*8+md5idx+1] = alpha[hash[hashidx]>>4*md5idx&0xf];
		}
	}

	return md5str;
}

md5_arith_t *md5_str2hash(md5_arith_t hash[const restrict static 4], const char md5str[restrict static 32]) {
	// need to handle invalid chars in md5str
	// if char is unsigned, hex digits may be >127 (POSIX.1-2001, 6.1)
	const uint8_t lettervals[CHAR_MAX+1] = { ['0']=0, ['1']=1, ['2']=2, ['3']=3,
	                                         ['4']=4, ['5']=5, ['6']=6, ['7']=7,
	                                         ['8']=8, ['9']=9, ['a']=10,
	                                         ['b']=11, ['c']=12, ['d']=13,
	                                         ['e']=14, ['f']=15, ['A']=10,
	                                         ['B']=11, ['C']=12, ['D']=13,
	                                         ['E']=14, ['F']=15
	                                       };

	for (uf8 hashidx=0; hashidx<4; ++hashidx) {
		hash[hashidx] = 0;
		for (uf8 md5idx=0; md5idx<8; md5idx+=2) {
			hash[hashidx] += (md5_arith_t)lettervals[md5str[hashidx*8+md5idx]&CHAR_MAX] << (md5idx+1)*4;
			hash[hashidx] += (md5_arith_t)lettervals[md5str[hashidx*8+md5idx+1]&CHAR_MAX] << md5idx*4;
		}
	}

	return hash;
}

uchar *md5_hash2ossl(uchar md5ossl[const restrict static 16], const md5_arith_t hash[restrict static 4]) {
	if (LE && sizeof(md5_arith_t) == 4)
		memcpy(md5ossl, hash, 16);
	else {
		uchar *out = md5ossl;
		// one-line for is slower
		for (uf8 i=0; i<4; ++i) {
			*out++ = hash[i];
			*out++ = hash[i] >> 8;
			*out++ = hash[i] >> 16;
			*out++ = hash[i] >> 24;
		}
	}

	return md5ossl;
}
