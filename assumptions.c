/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <float.h>
#include <stdlib.h>
#include <wchar.h>
#include "funcs.h"
#include "assumptions.h"



// assumes one of the three C99-allowed integer type representations
// \0-term.
uintmax_t int_precision(const char type[static 1]) {
	uintmax_t umax = 0, umax2;

	do {
		umax2 = umax;
		while (isspace((uchar)*type))
			++type;

		// every term which is a prefix of one or more other terms must be
		// checked after those terms
		if (! strncmp(type, "signed", 6)) {
			umax |= 0x1U;
			type += 6;
		}
		else if (! strncmp(type, "unsigned", 8)) {
			umax |= 0x2U;
			type += 8;
		}
		else if (! strncmp(type, "char", 4)) {
			umax |= 0x4U;
			type += 4;
		}
		else if (! strncmp(type, "short", 5)) {
			umax |= 0x8U;
			type += 5;
		}
		else if (! strncmp(type, "long", 4)) {
			if (! (umax & 0x20U))
				umax |= 0x20U;
			else umax |= 0x40U;
			type += 4;
		}
		else if (! strncmp(type, "intmax_t", 8)) {
			umax |= 0x80U;
			type += 8;
		}
		else if (! strncmp(type, "uintmax_t", 9)) {
			umax |= 0x100U;
			type += 9;
		}
		else if (! strncmp(type, "ssize_t", 7)) {
			umax |= 0x200U;
			type += 7;
		}
		else if (! strncmp(type, "size_t", 6)) {
			umax |= 0x400U;
			type += 6;
		}
		else if (! strncmp(type, "ptrdiff_t", 9)) {
			umax |= 0x800U;
			type += 9;
		}
		else if (! strncmp(type, "intptr_t", 8)) {
			umax |= 0x1000U;
			type += 8;
		}
		else if (! strncmp(type, "uintptr_t", 9)) {
			umax |= 0x2000U;
			type += 9;
		}
		else if (! strncmp(type, "int", 3)) {
			umax |= 0x10U;
			type += 3;
		}

		// duplicate/invalid term, term followed by a non-space character
		if (umax2 == umax || (*type && ! isspace((uchar)*type))) {
			umax = 0;
			break;
		}
	} while (*type);

	switch (umax) {
		case 0x4:
			if (CHAR_MAX == SCHAR_MAX && CHAR_MIN == SCHAR_MIN)
				umax = SCHAR_MAX;
			else if (CHAR_MAX == UCHAR_MAX && ! CHAR_MIN)
				umax = UCHAR_MAX;
			break;
		case 0x5: umax = SCHAR_MAX; break;
		case 0x6: umax = UCHAR_MAX; break;
		case 0x8: case 0x9: case 0x18: case 0x19: umax = SHRT_MAX; break;
		case 0xA: case 0x1A: umax = USHRT_MAX; break;
		case 0x10: case 0x11: umax = INT_MAX; break;
		case 0x2: case 0x12: umax = UINT_MAX; break;
		case 0x20: case 0x30: case 0x21: case 0x31: umax = LONG_MAX; break;
		case 0x22: case 0x32: umax = ULONG_MAX; break;
		case 0x60: case 0x70: case 0x61: case 0x71: umax = LLONG_MAX; break;
		case 0x62: case 0x72: umax = ULLONG_MAX; break;
		case 0x80: umax = INTMAX_MAX; break;
		case 0x100: umax = UINTMAX_MAX; break;
		case 0x200: umax = SSIZE_MAX; break;
		case 0x400: umax = SIZE_MAX; break;
		case 0x800: umax = PTRDIFF_MAX; break;
		case 0x1000: umax = INTPTR_MAX; break;
		case 0x2000: umax = UINTPTR_MAX; break;
		default: umax = 0; break;
	}

	return ubitwidth(umax);
}



// treat every type as signed, omitting the highest-order bit won't change the
// result
#define IS_PURELE(type) \
static bool CAT_EXP(is_pureLE_, type)(void) { \
	type var = 1; \
	size_t byte; \
	/* byte+1 to work around a false-positive warning */ \
	for (byte=0; byte+1<=sizeof(type)-1; ++byte) { \
		for (uchar i=1; i; i<<=1) { \
			if (((uchar *)&var)[byte] != i) \
				return false; \
			var <<= 1; \
		} \
	} \
	for (uchar i=1; i<128; i<<=1) { \
		if (((uchar *)&var)[byte] != i) \
			return false; \
		if (i < 64) \
			var <<= 1; \
	} \
	return true; \
}

#define IS_PUREBE(type) \
static bool CAT_EXP(is_pureBE_, type)(void) { \
	type var = 1; \
	size_t byte; \
	for (byte=sizeof(type)-1; byte; --byte) { \
		for (uchar i=1; i; i<<=1) { \
			if (((uchar *)&var)[byte] != i) \
				return false; \
			var <<= 1; \
		} \
	} \
	for (uchar i=1; i<128; i<<=1) { \
		if (((uchar *)&var)[0] != i) \
			return false; \
		if (i < 64) \
			var <<= 1; \
	} \
	return true; \
}

static bool is_pureLE_longlong(void) {
	long long var = 1;
	size_t byte;
	/* byte+1 to work around a false-positive warning */ \
	for (byte=0; byte+1<=sizeof(long long)-1; ++byte) {
		for (uchar i=1; i; i<<=1) {
			if (((uchar *)&var)[byte] != i)
				return false;
			var <<= 1;
		}
	}
	for (uchar i=1; i<128; i<<=1) {
		if (((uchar *)&var)[byte] != i)
			return false;
		if (i < 64)
			var <<= 1;
	}
	return true;
}

static bool is_pureBE_longlong(void) {
	long long var = 1;
	size_t byte;
	for (byte=sizeof(long long)-1; byte; --byte) {
		for (uchar i=1; i; i<<=1) {
			if (((uchar *)&var)[byte] != i)
				return false;
			var <<= 1;
		}
	}
	for (uchar i=1; i<128; i<<=1) {
		if (((uchar *)&var)[0] != i)
			return false;
		if (i < 64)
			var <<= 1;
	}
	return true;
}

IS_PURELE(short)
IS_PURELE(int)
IS_PURELE(long)
IS_PURELE(intmax_t)
IS_PURELE(intptr_t)
IS_PURELE(int8_t)
IS_PURELE(int16_t)
IS_PURELE(int32_t)
IS_PURELE(int64_t)
IS_PURELE(int_least8_t)
IS_PURELE(int_least16_t)
IS_PURELE(int_least32_t)
IS_PURELE(int_least64_t)
IS_PURELE(int_fast8_t)
IS_PURELE(int_fast16_t)
IS_PURELE(int_fast32_t)
IS_PURELE(int_fast64_t)
IS_PURELE(size_t)
IS_PURELE(ssize_t)
IS_PURELE(ptrdiff_t)
IS_PURELE(sig_atomic_t)
IS_PURELE(wchar_t)
IS_PURELE(wint_t)
IS_PUREBE(short)
IS_PUREBE(int)
IS_PUREBE(long)
IS_PUREBE(intmax_t)
IS_PUREBE(intptr_t)
IS_PUREBE(int8_t)
IS_PUREBE(int16_t)
IS_PUREBE(int32_t)
IS_PUREBE(int64_t)
IS_PUREBE(int_least8_t)
IS_PUREBE(int_least16_t)
IS_PUREBE(int_least32_t)
IS_PUREBE(int_least64_t)
IS_PUREBE(int_fast8_t)
IS_PUREBE(int_fast16_t)
IS_PUREBE(int_fast32_t)
IS_PUREBE(int_fast64_t)
IS_PUREBE(size_t)
IS_PUREBE(ssize_t)
IS_PUREBE(ptrdiff_t)
IS_PUREBE(sig_atomic_t)
IS_PUREBE(wchar_t)
IS_PUREBE(wint_t)



void make_assumptions(void) {
	// TODO: check for optional POSIX features
	// POSIX requires _XOPEN_SOURCE to be precisely 600
#if (__STDC__ != 1) || (__STDC_VERSION__ != 199901L) || (_XOPEN_SOURCE != 600) || (defined _GNU_SOURCE)
	abort();
#endif

	// CHAR_BIT is guaranteed to be 8 bits

	// Let's assume that for each integer type T it holds that *T is properly
	// aligned <=> its value is a multiple of sizeof(T). No way to verify.

	// int64_t, uint64_t exist. POSIX specifically recommends this check to
	// ascertain that.
#if (INT64_MAX != 9223372036854775807) || (UINT64_MAX != 18446744073709551615U)
	abort();
#endif

	// Selected unsigned types won't get int-promoted to int and become subject
	// to overflow. uintmax_t implicitly won't.
	if (((uintmax_t)SIZE_MAX & UINTPTR_MAX) <= INT_MAX)
		abort();

	// PTRDIFF_MAX is SIZE_MAX/2
	if (SIZE_MAX / 2 != PTRDIFF_MAX)
		abort();

	// ssize_t is indeed what it seems to be. Its minimum is probably
	// -SSIZE_MAX-1, but there's no way to find out and it's not important.
	if (SIZE_MAX / 2 != SSIZE_MAX)
		abort();

	// No integer type has any padding bits.
	//
	// (u)char and (u)intN_t are guaranteed to have no padding bits. Every
	// signed type (standard or extended) occupies the same amount of storage as
	// the corresponding unsigned type. The typedefs (u)int*_t from stdint.h are
	// guaranteed to form signed+unsigned pairs with this property.
	//
	// Let's assume that all types with no known maximum are typedefs of some of
	// the types below (no way to verify). Let's assume uintmax_t can hold the
	// bit width of any integer type.
	if ( ubitwidth(USHRT_MAX) != sizeof(short) * (uintmax_t)8 ||
	     ubitwidth(UINT_MAX) != sizeof(int) * (uintmax_t)8 ||
	     ubitwidth(ULONG_MAX) != sizeof(long) * (uintmax_t)8 ||
	     ubitwidth(ULLONG_MAX) != sizeof(long long) * (uintmax_t)8 ||
	     ubitwidth(UINTMAX_MAX) != sizeof(uintmax_t) * (uintmax_t)8 ||
	     ubitwidth(UINTPTR_MAX) != sizeof(uintptr_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_LEAST8_MAX) != sizeof(uint_least8_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_LEAST16_MAX) != sizeof(uint_least16_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_LEAST32_MAX) != sizeof(uint_least32_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_LEAST64_MAX) != sizeof(uint_least64_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_FAST8_MAX) != sizeof(uint_fast8_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_FAST16_MAX) != sizeof(uint_fast16_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_FAST32_MAX) != sizeof(uint_fast32_t) * (uintmax_t)8 ||
	     ubitwidth(UINT_FAST64_MAX) != sizeof(uint_fast64_t) * (uintmax_t)8 ||
	     ubitwidth(SIZE_MAX) != sizeof(size_t) * (uintmax_t)8 ||
	     ubitwidth(SSIZE_MAX) + 1 != sizeof(ssize_t) * (uintmax_t)8 ||
	     ubitwidth(PTRDIFF_MAX) + 1 != sizeof(ptrdiff_t) * (uintmax_t)8 ||
	     ubitwidth(SIG_ATOMIC_MAX) + (SIG_ATOMIC_MIN<0) != sizeof(sig_atomic_t) * (uintmax_t)8 ||
	     ubitwidth(WCHAR_MAX) + (WCHAR_MIN<0) != sizeof(wchar_t) * (uintmax_t)8 ||
	     ubitwidth(WINT_MAX) + (WINT_MIN<0) != sizeof(wint_t) * (uintmax_t)8
	   ) {
		abort();
	}

	// All signed integer types are in two's complement (implies no negative
	// zero).
	//
	// signed char and intN_t are guaranteed to be such. Two's complement is the
	// only representation allowed by C which affords the extra even negative
	// value.
	//
	// Let's assume that all types with no known minimum are typedefs of some of
	// the types below (no way to verify).
	if ( ( SHRT_MIN | INT_MIN | LONG_MIN | LLONG_MIN | INTMAX_MIN | INTPTR_MIN |
	       INT_LEAST8_MIN | INT_LEAST16_MIN | INT_LEAST32_MIN |
	       INT_LEAST64_MIN | INT_FAST8_MIN | INT_FAST16_MIN | INT_FAST32_MIN |
	       INT_FAST64_MIN | PTRDIFF_MIN | (intmax_t)SIG_ATOMIC_MIN |
	       (intmax_t)WCHAR_MIN | (intmax_t)WINT_MIN
	     ) % 2
	   ) {
		abort();
	}

	// All integer types are in pure binary notation. Moreover, all integer
	// types are in little-endian, or all integer types are in big-endian.
	//
	// (u)char is guaranteed to be in pure binary notation. Each value bit in
	// a signed type has the same meaning as the same bit in the corresponding
	// unsigned type.
	if ( is_pureLE_short() &&
	     is_pureLE_int() &&
	     is_pureLE_long() &&
	     is_pureLE_longlong() &&
	     is_pureLE_intmax_t() &&
	     is_pureLE_intptr_t() &&
	     is_pureLE_int8_t() &&
	     is_pureLE_int16_t() &&
	     is_pureLE_int32_t() &&
	     is_pureLE_int64_t() &&
	     is_pureLE_int_least8_t() &&
	     is_pureLE_int_least16_t() &&
	     is_pureLE_int_least32_t() &&
	     is_pureLE_int_least64_t() &&
	     is_pureLE_int_fast8_t() &&
	     is_pureLE_int_fast16_t() &&
	     is_pureLE_int_fast32_t() &&
	     is_pureLE_int_fast64_t() &&
	     is_pureLE_size_t() &&
	     is_pureLE_ssize_t() &&
	     is_pureLE_ptrdiff_t() &&
	     is_pureLE_sig_atomic_t() &&
	     is_pureLE_wchar_t() &&
	     is_pureLE_wint_t()
	   ) {
	}
	else if ( is_pureBE_short() &&
	          is_pureBE_int() &&
	          is_pureBE_long() &&
	          is_pureBE_longlong() &&
	          is_pureBE_intmax_t() &&
	          is_pureBE_intptr_t() &&
	          is_pureBE_int8_t() &&
	          is_pureBE_int16_t() &&
	          is_pureBE_int32_t() &&
	          is_pureBE_int64_t() &&
	          is_pureBE_int_least8_t() &&
	          is_pureBE_int_least16_t() &&
	          is_pureBE_int_least32_t() &&
	          is_pureBE_int_least64_t() &&
	          is_pureBE_int_fast8_t() &&
	          is_pureBE_int_fast16_t() &&
	          is_pureBE_int_fast32_t() &&
	          is_pureBE_int_fast64_t() &&
	          is_pureBE_size_t() &&
	          is_pureBE_ssize_t() &&
	          is_pureBE_ptrdiff_t() &&
	          is_pureBE_sig_atomic_t() &&
	          is_pureBE_wchar_t() &&
	          is_pureBE_wint_t()
	        ) {
	}
	else abort();

	// size_t can express the byte-size of anything in the virtual address
	// space that doesn't begin at the address 0.
	//
	// The contents of pointer types aren't defined, but it follows from the
	// universality of (void *) that it has at least as many possible values as
	// the pointer to any type.
	if (sizeof (size_t) < sizeof (void *))
		abort();

	// time_t can refer to current date+time and the near future.
	//
	// There's no portable way to tell the max of time_t. There's no lower limit
	// for its max either. Let's assume it's a typedef of a std int type, and
	// not an extended type and see that it has at least 31 value bits. 30 value
	// bits aren't enough to even represent the current date. time_t may be
	// signed or unsigned => it needs to have width >=32 bits.
	if (sizeof (time_t) < 4)
		abort();

	// long double can hold all integers in the range (-(2^64), 2^64) with no
	// divergence from their exact values
	if (LDBL_MANT_DIG < 64)
		abort();
}
