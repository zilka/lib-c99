# Copyright 2023 Roman Žilka
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.



OBJS ::= md5.o funcs-compat.o funcs.o assumptions.o
SRCS ::= $(OBJS:.o=.c)

CC = gcc
CFLAGS ::= -std=c99 -Wall -Wextra -pedantic -pipe -pthread
CPPFLAGS ::= -D_XOPEN_SOURCE=600 -Iarray -I.

# the thread sanitizer is incompatible with most others
#CFLAGS_ANALYZER ::= -fanalyzer -fanalyzer-verbosity=0
#CFLAGS_ANALYZER += -fsanitize={address,pointer-compare,pointer-subtract,leak,undefined} -fsanitize-sections=\* -fsanitize-address-use-after-scope
#CFLAGS_ANALYZER += -fsanitize={thread,undefined} -fsanitize-sections=\* -fsanitize-address-use-after-scope

# production
CFLAGS += -O3 -march=native
CPPFLAGS += -DNDEBUG
# assembler inspection
#CFLAGS += -O3
#CPPFLAGS += -DNDEBUG
# profiling
#CFLAGS += -O3 -g3 -pg
#CPPFLAGS += -DNDEBUG
# development Og
#CFLAGS += -Og -g3 -march=native $(CFLAGS_ANALYZER)
#CPPFLAGS += -D_FORTIFY_SOURCE=3
# development O0
#CFLAGS += -g3 -march=native $(CFLAGS_ANALYZER)



.PHONY: clean all

all: $(OBJS)

define GENDEPS =
RULE ::= $$(subst \ , ,$$(shell $(CC) $(CPPFLAGS) -MM $(1) 2>/dev/null))
DEPSDONE += $$(firstword $$(RULE))
$$(RULE)
endef
DEPSDONE ::=
$(foreach SRC,$(SRCS),$(eval $(call GENDEPS,$(SRC))))
ifneq ($(DEPSDONE::=),$(OBJS))
$(error Cannot run $(CC) -MM $(SRCS))
endif

$(OBJS): Makefile
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $(@:.o=.c)

clean:
	rm -f $(OBJS)
