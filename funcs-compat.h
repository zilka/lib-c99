/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#ifndef FUNCS_COMPAT_H_INCLUDED
#define FUNCS_COMPAT_H_INCLUDED

// The functions in this file are only allowed to use each other and libc.
// The prototypes are in sync with the SUS.

#include <stdio.h>
#include <search.h>

#if _POSIX_C_SOURCE < 200809L
#define getline(lineptr, n, stream) getdelim(lineptr, n, '\n', stream)
#endif

#ifndef _GNU_SOURCE
extern char *program_invocation_name;
extern char *program_invocation_short_name;
extern unsigned error_message_count;
extern void (*error_print_progname)(void);
#endif



void funcs_compat_init(char *argv0);
#if _POSIX_C_SOURCE < 200809L
size_t strnlen(const char *s, size_t maxlen);
char *strndup(const char *s, size_t size);
char *stpcpy(char *restrict s1, const char *restrict s2);
char *stpncpy(char *restrict s1, const char *restrict s2, size_t n);
ssize_t getdelim(char **restrict lineptr, size_t *restrict n, int delimiter, FILE *restrict stream);
#endif
#ifndef _GNU_SOURCE
char *canonicalize_file_name(const char *path);
char *strchrnul(const char *s, int c);
void *memrchr(const void *s, int c, size_t n);
void *memmem(const void *haystack, size_t haystacklen, const void *needle, size_t needlelen);
void *mempcpy(void *restrict dest, const void *restrict src, size_t n);
void error(int status, int errnum, const char *format, ...);
void explicit_bzero(void *s, size_t n);
void twalk_r(const void *root, void (*action)(const void *nodep, VISIT which, void *closure), void *closure);
void tdestroy(void *root, void (*free_node)(void *nodep));
#endif

#endif
