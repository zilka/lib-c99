/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#ifndef MD5_H_INCLUDED
#define MD5_H_INCLUDED

// RFC 1321

#include <stdint.h>
#include <limits.h>
#include <stdbool.h>

// Arith. in the main hashing pass is mod 2^32. Which is the fastest type that's
// unsigned, >=32 bits and larger than int (so it'll stay unsigned)?
#if ! defined(INT_MAX) || ! defined(UINT_FAST32_MAX) || ! defined(UINT_FAST64_MAX)
#error "Missing a *_MAX definition!"
#endif
#if INT_MAX < 4294967295  // 2^32-1
typedef uint32_t md5_arith_t;  // fastest by far
#elif UINT_FAST32_MAX > INT_MAX
typedef uint_fast32_t md5_arith_t;
#elif UINT_FAST64_MAX > INT_MAX
typedef uint_fast64_t md5_arith_t;
#else
typedef unsigned md5_arith_t;
#endif

// the fastest type for arith. mod 2^64
#if UINT_FAST64_MAX > INT_MAX
typedef uint_fast64_t md5_msgsize_t;
#else
typedef unsigned long long md5_msgsize_t;
#endif

#include "funcs.h"

typedef struct {
	// Registers: [0]=a, [1]=b, etc. Holds the digest itself in an unspecified
	// format which is only guaranteed constant as long as this code isn't
	// recompiled. Pass to the functions here. Don't use directly, except for
	// storing / copying around.
	md5_arith_t hash[4];  // public
	// Number of bytes hashed + buffered, mod 2^61 (in case input data is
	// larger). The original algorithm counts input bits in 64b, but that
	// count is only needed once and sizes arrive in bytes naturally.
	md5_msgsize_t msgsize;
	// Main buffer. Fastest when it's uint32_t, regardless of what *hash is. The
	// relative size to int doesn't matter. Fastest for both little- and
	// big-endian.
	uint32_t x[16];
	// bytes in x[] used (0-63)
	uf8 buffered;
} md5_digest;



// no function scrubs memory

// init() mandatory before any other op. At any later point (before or after
// final()) it resets state.
void md5_init(md5_digest dgst[const static 1]);
// call zero-many times to hash data, make size%64==0 for optimal performance
void md5_insert(md5_digest dgst[const restrict static 1], const uchar *restrict data, size_t size);
// after final() the digest is in dgst->hash
void md5_final(md5_digest dgst[const static 1]);

// equality test, 'hash' is md5_digest.hash
bool md5_equal(const md5_arith_t hash[static 4], const md5_arith_t hash2[static 4]);
// no \0 in 'md5str'
char *md5_hash2str(char md5str[const restrict static 32], const md5_arith_t hash[restrict static 4], bool capitals);
md5_arith_t *md5_str2hash(md5_arith_t hash[const restrict static 4], const char md5str[restrict static 32]);
// into the format used by, e.g., the RFC and OpenSSL
uchar *md5_hash2ossl(uchar md5ossl[const restrict static 16], const md5_arith_t hash[restrict static 4]);

#endif
