/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



// TODO: remove the \0-termination from things that don't desperately need it
// TODO: C99: functions in string.h may not be called on NULL even if
// the array's size is 0 - check everything
// TODO: an error() variant which works around EINTR in fprintf()

#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include "funcs-compat.h"
#include "funcs.h"

#define ARRAY_CHAR
#define ARRAY_PTR
#include "array.c"
#include "array.c"

const uf16 endianity_check_dummy_variable = 1;



// make sure the capacity of *p is >= 'ensure'
// a similar function in funcs-compat.c
void *ensalloc(void *restrict p[const restrict static 1], size_t size[const restrict static 1], size_t ensure) {
	assert((bool)*p == (bool)*size);
	assert(*size <= PTRDIFF_MAX);

	if (*size >= ensure) {
		// *p=NULL, *size=0, ensure=0: allocate 1 byte to honor the meaning of
		// the return value
		if (! *size) {
			*p = malloc(1);
			if (*p)
				*size = 1;
		}
		return *p;
	}
	assert(ensure);
	if (! fitzutu(ensure))
		return NULL;

	void *const new = realloc(*p, ensure);
	if (! new)
		return NULL;
	*p = new;
	*size = ensure;
	return *p;
}

// increase the capacity of *p by n
// a similar function in funcs-compat.c
void *growalloc(void *restrict p[const restrict static 1], size_t size[const restrict static 1], size_t n) {
	assert((bool)*p == (bool)*size);
	assert(*size <= PTRDIFF_MAX);

	if (! n) {
		// *p=NULL, *size=0, n=0: allocate 1 byte to honor the meaning of the
		// return value
		if (! *size) {
			*p = malloc(1);
			if (*p)
				*size = 1;
		}
		return *p;
	}
	assert(*size + n);
	if (! fit2zutu(*size, n))
		return NULL;

	void *const new = realloc(*p, *size+n);
	if (! new)
		return NULL;
	*p = new;
	*size += n;
	return *p;
}



// in SUSv3 the length of the link tgt can be 0 apparently (changed later)
// 'st_size' == -1 => unspecified (will lstat())
// unlike readlink(), the result is \0-term.
// errno: ensalloc(), growalloc(), readlink(), EOVERFLOW, ENOMEM
// on failure dest and destsize consistent, but unspecified
// an exact copy in funcs-compat.c, keep str and size separate
ssize_t readlink_full(const char path[restrict static 1], off_t st_size, char *restrict dest[const restrict static 1], size_t destsize[const restrict static 1]) {
	assert((bool)*dest == (bool)*destsize);

	// st_size sanity check because of virtual filesystems et al.
	if (st_size != -1)
		ensalloc((void **)dest, destsize, st_size>0 && st_size<1024 ? st_size+1 : 64);
	else {
		struct stat meta;
		if (! lstat(path, &meta) && meta.st_size > 0 && meta.st_size < 1024)
			ensalloc((void **)dest, destsize, meta.st_size+1);
		else ensalloc((void **)dest, destsize, 64);
	}
	if (! ensalloc((void **)dest, destsize, 1))
		return -1;

	for (;;) {
		assert(SSIZE_MAX == PTRDIFF_MAX);
		// reading must be into a buffer that's not \0 terminated - that would
		// limit the maximum name the original readlink() can process
		ssize_t linklen = readlink(path, *dest, *destsize);  // no \0
		if (linklen == -1)
			return -1;
		// there actually is space for the '\0', because a link target SSIZE_MAX
		// chars long will be interpreted as truncated
		if ((size_t)linklen < *destsize) {
			(*dest)[linklen] = '\0';
			return linklen;
		}
		// also covers linklen=SSIZE_MAX (=> *destsize=SSIZE_MAX)
		if ( ! growalloc((void **)dest, destsize, 64) &&
		     ! growalloc((void **)dest, destsize, 1)
		   ) {
			return -1;
		}
	}
}



// on failure dest and destsize consistent, but unspecified
// an exact copy in funcs-compat.c, keep str and size separate
char *getcwd_full(char *restrict dest[const restrict static 1], size_t destsize[const restrict static 1]) {
	assert((bool)*dest == (bool)*destsize);

	if ( ! ensalloc((void **)dest, destsize, 64) &&
	     ! ensalloc((void **)dest, destsize, 1)
	   ) {
		return NULL;
	}

	// getcwd() returns a canonical path, output includes a \0
	while (! getcwd(*dest, *destsize)) {
		if ( errno != ERANGE ||
		     ( ! growalloc((void **)dest, destsize, 64) &&
		       ! growalloc((void **)dest, destsize, 1)
		     )
		   ) {
			return NULL;
		}
	}

	if ((*dest)[0] != '/') {
		errno = ENOENT;  // fix the "(unreachable)" issue with glibc < 2.27
		return NULL;
	}
	return *dest;
}



// s://+:/:g, \0-term., can be empty
// trailing => all slashes at the end are removed
// returns the length of the resulting 's'
// The reason for "const s" is that it's an input+output param. and both of
// these directions are mandatory. The caller must pass a valid value in `s' and
// the function must return something through that same pointer. That means it
// must not lose track of the pointer.
size_t path_trimsl(char s[const static 1], bool trailing) {
	char *p, *pend = s;
	size_t slen = strlen(s);

	while (( p = strstr(pend, "//") )) {
		++p;
		for (pend=p+1; *pend=='/';)
			++pend;
		memmove(p, pend, slen-(pend-s)+1);  // it'll almost never happen
		slen -= pend - p;
	}

	// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108154
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
	if (trailing && slen > 1 && s[slen-1] == '/')
#pragma GCC diagnostic ignored "-Wstringop-overflow"
		s[--slen] = '\0';
#pragma GCC diagnostic pop
	return slen;
}



// \0-term., can be empty
inline bool isdotdir(const char name[static 1]) {
	// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108154
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
	return name[0] == '.' && (! name[1] || (name[1] == '.' && ! name[2]));
#pragma GCC diagnostic pop
}

// \0-term., can be empty
// returns the start of the first occurrence of a "."/"..", or NULL
const char *hasdotdir(const char path[static 1]) {
	do {
		if ( path[0] == '.' &&
		     ( ! path[1] || path[1] == '/' ||
		       (path[1] == '.' && (! path[2] || path[2] == '/'))
		     )
		   ) {
			return path;
		}
		path = strchr(path, '/');
	} while (path++);

	return NULL;
}

const char *hasdotdirn(const char path[static 1], size_t len) {
	const char *next;

	while (len) {
		if ( path[0] == '.' &&
		     ( len == 1 || path[1] == '/' ||
		       (path[1] == '.' && (len == 2 || path[2] == '/'))
		     )
		   ) {
			return path;
		}
		next = memchr(path, '/', len);
		if (! next)
			return NULL;
		len -= ++next - path;
		path = next;
	};

	return NULL;
}



// Like canonicalize_file_name(), except:
// - if the basename of 'path' isn't "." or "..", it is left unresolved - that
//   will allow the caller to refer to a symlink in an otherwise resolved dir
// - the existence of basename (other than "."/"..") inside dirname on disk is
//   hence not confirmed
// - if canonicalize_file_name() on the dirname fails (e.g., it doesn't exist),
//   this func fails too
// - if there's a trailing slash(es) following a basename other than "."/"..",
//   the slash (just one) is preserved to maintain information about the type of
//   the basename
char *canonicalize_dirname(const char *path) {
	if (! path) {
		errno = EINVAL;
		return NULL;
	}
	if (! *path) {
		errno = ENOENT;
		return NULL;
	}

	const char *pathend = path + strlen(path);
	const char *basename = pathend - 1;

	if (*basename == '/') {  // skip all trailing slashes except one
		do {
			if (basename == path)
				return strdup("/");  // only slash(es)
			--basename;
		} while (*basename == '/');
		pathend = basename + 2;
	}

	// make sure that the path that goes into canonicalize_file_name() is
	// designated as a dir (ends in one of the "."/".." or a '/'), so that it
	// only resolves successfully in such case
	if (*basename == '.') {
		// basename or whole path = "." (plus slashes, maybe)
		if (basename == path || basename[-1] == '/')
			return canonicalize_file_name(path);
		// basename or whole path = ".." (plus slashes, maybe)
		if (basename[-1] == '.' && (basename-1==path || basename[-2]=='/'))
			return canonicalize_file_name(path);
	}

	while (basename != path && basename[-1] != '/') --basename;
	// basename now final, nonempty, non-"."/"..", there may or may not be
	// something before it

	char *dirname;
	dirname = basename==path ? strdup(".") : strexndup(basename-path,path);
	if (! dirname)
		return NULL;
	// dirname now final, nonempty (the inserted "./" at the very least)

	int err = 0;
	char *tgt = canonicalize_file_name(dirname);
	if (! tgt)
		err = errno;
	else {
		char *p;
		const size_t tgtlen = strlen(tgt) - (! tgt[1]);
		const size_t basenamelen = pathend - basename;
		if ( fit2zuzu(tgtlen+1, basenamelen+1) &&
		     (p = realloc(tgt, tgtlen+basenamelen+2))
		   ) {
			tgt = p;
			tgt[tgtlen] = '/';
			memcpy(tgt+(tgtlen+1), basename, basenamelen);
			tgt[tgtlen+1+basenamelen] = '\0';
		} else err = errno;
	}

	free(dirname);
	if (err) {
		free(tgt);
		tgt = NULL;
		errno = err;
	}
	return tgt;
}



// like strndup, but exactly 'len' bytes (no check)
char *strexndup(size_t len, const char s[static 1]) {
	if (! fit2zutu(len, 1))
		return NULL;
	char *const new = malloc(++len);
	if (! new)
		return NULL;
	*(char *)mempcpy(new, s, len-1) = '\0';
	return new;
}

// like strncpy(), but always terminates 's' and doesn't fill the rest of 's'
// with zeros
// requires !=NULL ptrs, like strncpy() and all string.h funcs
char *strncpy2(char dest[const restrict static 1], const char src[restrict static 1], size_t n) {
	if (n) {
		if (! memccpy(dest, src, '\0', n-1))
			dest[n-1] = '\0';
	}
	return dest;
}



// Optimal I/O op. size, fits in ssize_t and ptrdiff_t too.
// 'blksize' is fd's st_blksize, -1 => unspecified (will fstat()).
// 'fd' always needs to be actual, in case st_blksize is a natural -1.
size_t iosize(int fd, blksize_t blksize) {
	if (blksize != -1) {
		if (blksize <= 0 || blksize > SSIZE_MAX)
			return IOSIZE;
		return blksize;
	}

	struct stat meta;
	if (fstat(fd, &meta) || meta.st_blksize <= 0 || meta.st_blksize > SSIZE_MAX)
		return IOSIZE;
	return meta.st_blksize;
}



// 'blksize' is path's st_blksize, -1 => unspecified (will stat())
// returns 'out' or NULL
// on error, 'out' contents undefined, EINTR possible
// doesn't scrub memory
md5_arith_t *md5(const char path[restrict static 1], blksize_t blksize, md5_arith_t out[const static 4]) {
	int err = 0;

	FILE *const file = fopen(path, "r");

	if (! file)
		err = errno;
	else {
		setbuf(file, NULL);
		const size_t bufsize = iosize(fileno(file), blksize);
		void *const buf = malloc(bufsize);

		if (! buf)
			err = errno;
		else {
			md5_digest ctx;
			md5_init(&ctx);

			for (;;) {
				/* If there's >0, but <memb_size (2nd param) bytes left to read
				 * fread() advances to EOF in the file, but discards the
				 * remaining data + returns 0. Using memb_size=1 is OK, though -
				 * - the compiled code read()s in chunks, not single bytes. */
				size_t nr = fread(buf, 1, bufsize, file);
				// SUSv3: ferror() doesn't communicate error in errno, but
				// there's no guarantee it won't change errno
				err = errno;
				// ferror() first, regardless of 'nr'
				if (ferror(file))
					break;
				else if (! nr) {  // successfully finished reading
					// zero it, because a non-zero errno could've been an
					// innocuous side-effect of fread() or something earlier
					err = 0;
					md5_final(&ctx);
					memcpy(out, ctx.hash, sizeof(ctx.hash));
					break;
				}
				else md5_insert(&ctx, buf, nr);
			}

			free(buf);
		}

		if (fclose(file))
			assert(! "Unexpected error at fclose()");
	}

	if (err) {
		errno = err;
		return NULL;
	}
	return out;
}



// Calls the GNU-specific strerror_r() or emulates it if it isn't available.
// Accepts the XSI version of strerror_r() from glibc < 2.13.
char *err2str(int err, char buf[const static 1], size_t bufsize) {
#ifndef _GNU_SOURCE
	int a = strerror_r(err, buf, bufsize);
	if (! a)
		return buf;

	// it's unspecified whether strerror_r() first checks (and returns) for
	// ERANGE or EINVAL
	if ((a == ERANGE || (a == -1 && errno == ERANGE)) && bufsize < 1024) {
		char buf2[1024];
		// ugly (copying), but this shouldn't happen often
		if (!( a = strerror_r(err, buf2, sizeof(buf2)) ))
			return strncpy2(buf, buf2, bufsize);
	}
	// can still be an EINVAL or an ERANGE
	assert(a != ERANGE && (a != -1 || errno != ERANGE));
	snprintf(buf, bufsize, "Unknown error %d", err);  // matches glibc

	return buf;
#else
	return strerror_r(err, buf, bufsize);
#endif
}



// RFC 4648
// l64a() isn't thread-safe
size_t base64enc(size_t srcsize, const uchar *restrict src, char *const restrict dest) {
	assert(! srcsize || (src && dest));
	// for every next sequence of three input bytes four bytes are output
	assert(fitprod2zutu(srcsize/3+(srcsize%3>0), 4));

	const char *const table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	const uf8 srctail = srcsize % 3;
	const uchar *const srcend = src+(srcsize-srctail);
	char *next = dest;

	for (; src<srcend; src+=3) {
		*next++ = table[src[0]>>2];
		*next++ = table[(src[0]&0x3U)<<4|src[1]>>4];
		*next++ = table[(src[1]&0xfU)<<2|src[2]>>6];
		*next++ = table[src[2]&0x3fU];
	}
	if (srctail) {
		*next++ = table[src[0]>>2];
		if (srctail == 1) {
			*next++ = table[(src[0]&0x3U)<<4];
			*next++ = '=';
		}
		else {
			*next++ = table[(src[0]&0x3U)<<4|src[1]>>4];
			*next++ = table[(src[1]&0xfU)<<2];
		}
		*next++ = '=';
	}

	return next - dest;
}



// stable mergesort, stability of qsort() is unspecified
void stablesort(void *const base, size_t nritems, size_t itemsize, int (*cmp)(const void *, const void *)) {
	assert(fitprod2zutu(nritems, itemsize));
	const size_t arraysize = nritems * itemsize;
	assert(! arraysize || base);

	if (arraysize <= 1)
		return;
	void *base2 = malloc(arraysize);
	if (! base2)
		return;
	// the top-most cycle will run at least once, no need to memcpy to base2[]

	size_t seglen, merged, lseg_done, rseg_done, rseg_len;
	uchar *in = base;
	uchar *out = base2;
	seglen = 1;

	while (true) {
		assert(nritems > seglen);
		// is there still more than one full segment left?
		// addition safe, there can never be more merged items than nritems
		for (merged=0; nritems-merged>seglen; merged+=seglen+rseg_len) {
			// lseg start idx = merged
			// lseg len = seglen
			// rseg start idx = merged+seglen
			// rseg len = min(seglen, nritems-merged-seglen)i
			lseg_done = rseg_done = 0;
			if (seglen <= nritems-merged-seglen)
				rseg_len = seglen;  // most of the time
			else rseg_len = nritems-merged-seglen;
			// lseg len > 0, rseg len > 0
			while (lseg_done < seglen && rseg_done < rseg_len) {
				// if (in[merged+lseg_done] <= in[merged+seglen+rseg_done]) {
				// 	out[merged+lseg_done+rseg_done] = in[merged+lseg_done];
				// 	++lseg_done;
				// }
				// else {
				// 	out[merged+lseg_done+rseg_done] = in[merged+seglen+rseg_done];
				// 	++rseg_done;
				// }
				if (cmp(in+itemsize*(merged+lseg_done), in+itemsize*(merged+seglen+rseg_done)) <= 0) {
					memcpy(out+itemsize*(merged+lseg_done+rseg_done), in+itemsize*(merged+lseg_done), itemsize);
					++lseg_done;
				}
				else {
					memcpy(out+itemsize*(merged+lseg_done+rseg_done), in+itemsize*(merged+seglen+rseg_done), itemsize);
					++rseg_done;
				}
			}
			if (lseg_done < seglen)
				memcpy(out+itemsize*(merged+lseg_done+rseg_done), in+itemsize*(merged+lseg_done), itemsize*(seglen-lseg_done));
			else memcpy(out+itemsize*(merged+lseg_done+rseg_done), in+itemsize*(merged+seglen+rseg_done), itemsize*(rseg_len-rseg_done));
		}
		if (merged < nritems)
			memcpy(out+itemsize*merged, in+itemsize*merged, itemsize*(nritems-merged));

		if (SIZE_MAX-seglen < seglen || (seglen*=2) >= nritems)
			break;

		base2=in; in=out; out=base2;
	}

	if (base == in) {
		memcpy(base, out, arraysize);
		free(out);
	}
	else free(in);
}



// bsearch() "may reorder elements of the array between calls to the comparison
// function". Does it mean that ...?
// Doesn't demand key!=NULL.
void *bsearch2(const void *key, const void *base, size_t nel, size_t width, int (*compar)(const void *, const void *)) {
	assert(! nel || (key && base && width && compar));

    size_t min=1, next;
    assert(min > 0 && nel < SIZE_MAX);  // can always go one step past min, nel
    int relation;

    while (min <= nel) {
        next = min-1 + (nel-min) / 2;
        assert(next+1 >= min && next+1 <= nel);
        relation = compar(key, (uchar *)base+width*next);
        if (relation == 0)
            return (uchar *)base+width*next;
        if (relation < 0)
            nel = next;
        else min = next + 2;
    }
    return NULL;
}



// RFC 3629, Unicode v15.0
inline uf8 u8charsize(const uchar s[static 1], size_t size) {
	assert(size);

	// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108154
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
	// 0xxxxxxx, U+0000 - U+007F
	if (s[0] <= 0x7f) return 1;
	if (size >= 2 && (s[1]&0xc0) == 0x80) {
		// 110xxxxx 10xxxxxx, U+0080 - U+07FF
		if (s[0] >= 0xc2 && s[0] <= 0xdf) return 2;
		if (size >= 3 && (s[2]&0xc0) == 0x80) {
			const uf16 x = (uf16)s[0] << 6 | (s[1] & 0x3f);
			// 1110xxxx 10xxxxxx 10xxxxxx, U+0800 - U+FFFF minus U+D800 - U+DFFF
			if ((x >= 0x3820 && x <= 0x3b5f) || (x >= 0x3b80 && x <= 0x3bff)) return 3;
			if (size >= 4 && (s[3]&0xc0) == 0x80) {
				// 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx, U+010000 - U+10FFFF
				if (x >= 0x3c10 && x <= 0x3d0f) return 4;
			}
		}
	}
#pragma GCC diagnostic pop

	// invalid or incomplete sequence
	return 0;
}



// Number of bits necessary to represent n. result <= n.
uintmax_t ubitwidth(uintmax_t n) {
	uintmax_t w = 0;
	for (; n; n>>=1)
		++w;
	return w;
}



inline bool fit2zutu(size_t a, size_t b) {
	if ((a|b) <= PTRDIFF_MAX && a+b <= PTRDIFF_MAX)
		return true;
	errno = EOVERFLOW;
	return false;
}

inline bool fitprod2zuzu(size_t a, size_t b) {
	if (! a || SIZE_MAX/a >= b)
		return true;
	errno = EOVERFLOW;
	return false;
}

inline bool fitprod2zutu(size_t a, size_t b) {
	if (! a || PTRDIFF_MAX/a >= b)
		return true;
	errno = EOVERFLOW;
	return false;
}



/*
// Sum of the 'n' integers of type size_t in 'ap'.
// Overflow => returns SIZE_MAX and sets errno=EOVERFLOW. Won't set errno for
// any other reason.
size_t sum_sizet(unsigned n, va_list ap) {
	size_t next;
	uintmax_t total = 0;

	while (n--) {
		next = va_arg(ap, size_t);
		// size_t may be smaller than int and get promoted => acc. in uintmax_t
		if (total+next < total || (total+=next) > SIZE_MAX) {
			errno = EOVERFLOW;
			return SIZE_MAX;
		}
	}

	return total;
}
*/
