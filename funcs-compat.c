/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include "funcs-compat.h"

#ifndef _GNU_SOURCE
typedef struct {
	void (*action)(const void *nodep, VISIT which, void *closure);
	void *closure;
} twalkr_data_t;

char *program_invocation_name;
char *program_invocation_short_name;
unsigned error_message_count = 0;
void (*error_print_progname)(void) = NULL;

static pthread_key_t thread_local_twalkr_data;
#endif



// call in main() before everything else as funcs_compat_init(argv[0])
void funcs_compat_init(char *argv0) {
#ifndef _GNU_SOURCE
	program_invocation_name = argv0;
	program_invocation_short_name = strrchr(argv0, '/');
	if (program_invocation_short_name)
		++program_invocation_short_name;
	else program_invocation_short_name = argv0;

	if (pthread_key_create(&thread_local_twalkr_data, NULL))
		abort();
#else
	(void)argv0;
#endif
}



// a rough copy of ensalloc() from funcs.c, static
#if (_POSIX_C_SOURCE < 200809L) || (! defined _GNU_SOURCE)
static void *ensalloc(void *restrict p[const restrict static 1], size_t size[const restrict static 1], size_t ensure) {
	if (*size >= ensure) {
		// *p=NULL, *size=0, ensure=0: allocate 1 byte to honor the meaning of
		// the return value
		if (! *size) {
			*p = malloc(1);
			if (*p)
				*size = 1;
		}
		return *p;
	}
	if (ensure > PTRDIFF_MAX) {
		errno = EOVERFLOW;
		return NULL;
	}

	void *const new = realloc(*p, ensure);
	if (! new)
		return NULL;
	*p = new;
	*size = ensure;
	return *p;
}
#endif

// a rough copy of growalloc() from funcs.c, static
#if (_POSIX_C_SOURCE < 200809L) || (! defined _GNU_SOURCE)
static void *growalloc(void *restrict p[const restrict static 1], size_t size[const restrict static 1], size_t n) {
	if (! n) {
		// *p=NULL, *size=0, n=0: allocate 1 byte to honor the meaning of the
		// return value
		if (! *size) {
			*p = malloc(1);
			if (*p)
				*size = 1;
		}
		return *p;
	}
	if (SIZE_MAX - *size < n || *size + n > PTRDIFF_MAX) {
		errno = EOVERFLOW;
		return NULL;
	}

	void *const new = realloc(*p, *size+n);
	if (! new)
		return NULL;
	*p = new;
	*size += n;
	return *p;
}
#endif



// an copy of readlink_full() from funcs.c, static
#if (_POSIX_C_SOURCE < 200809L) || (! defined _GNU_SOURCE)
static ssize_t readlink_full(const char path[restrict static 1], off_t st_size, char *restrict dest[const restrict static 1], size_t destsize[const restrict static 1]) {
	// st_size sanity check because of virtual filesystems et al.
	if (st_size != -1)
		ensalloc((void **)dest, destsize, st_size>0 && st_size<1024 ? st_size+1 : 64);
	else {
		struct stat meta;
		if (! lstat(path, &meta) && meta.st_size > 0 && meta.st_size < 1024)
			ensalloc((void **)dest, destsize, meta.st_size+1);
		else ensalloc((void **)dest, destsize, 64);
	}
	if (! ensalloc((void **)dest, destsize, 1))
		return -1;

	for (;;) {
		// reading must be into a buffer that's not \0 terminated - that would
		// limit the maximum name the original readlink() can process
		ssize_t linklen = readlink(path, *dest, *destsize);  // no \0
		if (linklen == -1)
			return -1;
		// there actually is space for the '\0', because a link target SSIZE_MAX
		// chars long will be interpreted as truncated
		if ((size_t)linklen < *destsize) {
			(*dest)[linklen] = '\0';
			return linklen;
		}
		// also covers linklen=SSIZE_MAX (=> *destsize=SSIZE_MAX)
		if ( ! growalloc((void **)dest, destsize, 64) &&
		     ! growalloc((void **)dest, destsize, 1)
		   ) {
			return -1;
		}
	}
}
#endif



// an copy of getcwd_full() from funcs.c, static
#if (_POSIX_C_SOURCE < 200809L) || (! defined _GNU_SOURCE)
static char *getcwd_full(char *restrict dest[const restrict static 1], size_t destsize[const restrict static 1]) {
	if ( ! ensalloc((void **)dest, destsize, 64) &&
	     ! ensalloc((void **)dest, destsize, 1)
	   ) {
		return NULL;
	}

	// getcwd() returns a canonical path, output includes a \0
	while (! getcwd(*dest, *destsize)) {
		if ( errno != ERANGE ||
		     ( ! growalloc((void **)dest, destsize, 64) &&
		       ! growalloc((void **)dest, destsize, 1)
		     )
		   ) {
			return NULL;
		}
	}

	if ((*dest)[0] != '/') {
		errno = ENOENT;  // fix the "(unreachable)" issue with glibc < 2.27
		return NULL;
	}
	return *dest;
}
#endif



#if _POSIX_C_SOURCE < 200809L
size_t strnlen(const char *s, size_t maxlen) {
	const char *const p = memchr(s, '\0', maxlen);  // maxlen=0 => NULL
	return p ? (size_t)(p-s) : maxlen;
}
#endif

// 'n' = 0 or 's' = "" ==> returns a newly allocated empty string
// does not check for n==SIZE_MAX
#if _POSIX_C_SOURCE < 200809L
char *strndup(const char *s, size_t size) {
	size = strnlen(s, size);
	char *const res = malloc(size + 1);
	if (! res)
		return NULL;
	memcpy(res, s, size);
	res[size] = '\0';
	return res;
}
#endif

#if _POSIX_C_SOURCE < 200809L
char *stpcpy(char *restrict s1, const char *restrict s2) {
	// don't read past s2
	return (char *)mempcpy(s1, s2, strlen(s2)+1) - 1;
}
#endif

#if _POSIX_C_SOURCE < 200809L
char *stpncpy(char *restrict s1, const char *restrict s2, size_t n) {
	char *const p = memccpy(s1, s2, '\0', n);
	if (! p)
		return s1 + n;
	memset(p, 0, n - (p-s1));
	return p - 1;
}
#endif



// Exactly one of feof/ferror should be set on the stream on -1. Setting ferror
// in FILE isn't directly possible, so ferror() isn't a reliable tell. So make
// sure that feof()=true => no ferror (even if it were possible to set).
// https://sourceware.org/bugzilla/show_bug.cgi?id=29917
// https://austingroupbugs.net/view.php?id=1624
#if _POSIX_C_SOURCE < 200809L
ssize_t getdelim(char **restrict lineptr, size_t *restrict n, int delimiter, FILE *restrict stream) {
	if (feof(stream))
		return -1;  // feof && !ferror
	if (! lineptr || ! n) {
		errno = EINVAL;
		return -1;  // !feof && ferror
	}
	if (! *lineptr || ! *n) {
		*n = 0;
		*lineptr = NULL;
		// Pre-allocate and always keep 1 byte free for the '\0. That is to
		// avoid the possibility of reading successfully till EOF and then
		// failing on ENOMEM (feof() and an error cannot occur simultaneously).
		if ( ! ensalloc((void **)lineptr, n, 128) &&
		     ! ensalloc((void **)lineptr, n, 1)
		   ) {
			return -1;  // !feof && ferror
		}
	}

	size_t len = 0;
	int c;

	do {
		// Go ahead and make use of ungetc(). True, it can only be called once
		// and caller may want to use it. But it's only used here when ENOMEM
		// happens and in that case the stream is in an indeterminate state
		// anyway (per POSIX, it should have the error indicator set, in fact),
		// so it doesn't matter that the ungetc is used up.
		c = getc(stream);  // getc() et al. do set errno
		if (c == EOF) {
			int e = errno;
			if (feof(stream)) {
				if (! len)
					return -1;  // feof && !ferror
				break;
			}
			errno = e;
			return -1;  // !feof && ferror
		}
		if ( len == *n-1 &&
		     ! growalloc((void **)lineptr, n, 128) &&
		     ! growalloc((void **)lineptr, n, 1)
		   ) {
			int e = errno;
			ungetc(c, stream);
			errno = e;
			return -1;  // !feof && ferror
		}
		((unsigned char *)*lineptr)[len++] = c;
	} while (c != delimiter);

	(*lineptr)[len] = '\0';
	return len;
}
#endif



// fails if 'path' doesn't exist
// does take the trailing '/' into consideration
#ifndef _GNU_SOURCE
char *canonicalize_file_name(const char *path) {
	// No PATH_MAX/NAME_MAX checks are made directly on 'path', even though
	// according to SUS they probably should be. Checks on the result are
	// provided by getcwd_full() and lstat().
	if (! path) {
		errno = EINVAL;
		return NULL;
	}
	if (! *path) {
		errno = ENOENT;
		return NULL;
	}

	int err = 0;
	char *newpath = NULL, *res = NULL, *linktgt = NULL;
	size_t pathlen, ressize = 0, reslen, linktgtsize = 0, siz;
	ssize_t linktgtlen;
	long symloopmax = 0, eloopctr = 0;
	struct stat meta;
	bool nondirseg = false;

	if (*path == '/') {
		// an actual path => satisfies PATH_MAX
		res = strdup("/");
		if (! res)
			return NULL;
		ressize = 2;
		reslen = 1;
		while (*++path == '/') ;
	}
	else {
		// returns an actual path => satisfies PATH_MAX/NAME_MAX implicitly
		if (! getcwd_full(&res, &ressize)) {
			err = errno;
			free(res);
			errno = err;
			return NULL;
		}
		reslen = strlen(res);
	}
	// 'path' is past the initial '/'s, if there were any
	// 'res' initialized

	/* At the loop beginning:
	 * - 'path' is at the beginning of the next path segm., after the '/'
	 * - we make no assumptions about what's in the string before 'path'
	 * - res is nonempty and it's either exactly "/", or not '/'-terminated
	 * - res is, in fact, canonical and may be the final result */
	while (*path) {
		for (pathlen=0; path[pathlen] && path[pathlen]!='/';)
			++pathlen;

		if (pathlen == 2 && path[0] == '.' && path[1] == '.') {
			// pop segment from result path
			if (reslen > 1) {
				reslen = strrchr(res,'/') - res;
				if (! reslen)
					reslen = 1;
				res[reslen] = '\0';
			}
		}
		else if (pathlen != 1 || path[0] != '.') {  // "." ==> NOP
			// push segment to result path
			if (reslen > 1) {
				// each of these operands fits (both're lengths)
				if (SIZE_MAX - (reslen+1) < (pathlen+1)) {
					err = ENOMEM;
					break;
				}
				reslen += 1 + pathlen;
				if (! ensalloc((void **)&res, &ressize, reslen+1)) {
					err = errno;
					break;
				}
				res[reslen-pathlen-1] = '/';
				memcpy(res+(reslen-pathlen), path, pathlen);
			}
			else {
				if (pathlen + 1 == SIZE_MAX) {
					err = ENOMEM;
					break;
				}
				reslen = pathlen + 1;
				if (! ensalloc((void **)&res, &ressize, reslen+1)) {
					err = errno;
					break;
				}
				memcpy(res+1, path, pathlen);
			}
			res[reslen] = '\0';

			// There are no tricks with opendir() if stat() fails! Matches
			// realpath() as well as path_resolution(7). Moreover, opendir()
			// would succeed not just on a dir, but also a symlink -> dir.
			if (lstat(res, &meta)) {
				err = errno;
				break;
			}
			else if (! S_ISLNK(meta.st_mode)) {
				if (! S_ISDIR(meta.st_mode))
					nondirseg = true;
			}
			else {
				// TODO: SUS/realpath: it must detect loops.
				// TODO: sysconf(SYMLOOP_MAX) may also be without limit (-1,
				// no errno).
				// can't be static (thread-safety), sysconf() probably isn't
				// slower than pthread_once()
				if (! symloopmax) {
					symloopmax = sysconf(_SC_SYMLOOP_MAX);
					// there is no XOPEN limit for SYMLOOP_MAX
					if (symloopmax < _POSIX_SYMLOOP_MAX)
						symloopmax = _POSIX_SYMLOOP_MAX;
				}

				if (eloopctr++ == symloopmax) {
					err = ELOOP;
					break;
				}

				// 'res' isn't \0-term.
				linktgtlen = readlink_full(res, meta.st_size, &linktgt, &linktgtsize);
				if (linktgtlen == -1) {
					err = errno;
					break;
				}
				if (*linktgt == '/')
					reslen = 1;  // strip all segments
				else {
					// pop segment from result path (contains 'path' on top)
					reslen -= pathlen;
					if (reslen > 1)
						--reslen;
				}
				res[reslen] = '\0';

				siz = strlen(path+pathlen) + 1;
				if (SIZE_MAX - linktgtlen < siz) {
					err = ENOMEM;
					break;
				}
				if (! ensalloc((void **)&linktgt, &linktgtsize, linktgtlen+siz)) {
					err = errno;
					break;
				}
				memcpy(linktgt+linktgtlen, path+pathlen, siz);
				free(newpath);
				// outside of this remaining segment linktgt will always be NULL
				// or !=newpath
				newpath = linktgt;
				path = newpath;
				pathlen = 0;
				linktgt = NULL;
				linktgtsize = 0;
			}
		}

		path += pathlen;
		if (nondirseg && *path) {
			err = ENOTDIR;
			break;
		}
		while (*path == '/')
			++path;
	}
	// (! *path) <==> the loop was successful
	// the loop was successful <==> err isn't set

	free(newpath);
	if (err) {
		free(linktgt);  // always NULL on main loop success
		free(res);
		res = NULL;
		errno = err;
	}
	return res;
}
#endif



#ifndef _GNU_SOURCE
char *strchrnul(const char *s, int c) {
	const size_t l = strlen(s);
	char *const p = memchr(s, c, l+1);
	return p ? p : (char *)s + l;
}
#endif

#ifndef _GNU_SOURCE
// TODO: probably faster to start from the back
void *memrchr(const void *s, int c, size_t n) {
	const char *next = memchr(s, c, n);
	if (! next)
		return NULL;

	do {
		++next;
		n -= next - (const char *)s;
		s = next;
	} while (( next = memchr(s, c, n) ));
	return (char *)s - 1;
}
#endif

#ifndef _GNU_SOURCE
void *memmem(const void *haystack, size_t haystacklen, const void *needle, size_t needlelen) {
	if (! needlelen)
		return NULL;
	const unsigned char *cand;

	while ( haystacklen >= needlelen &&
	        (cand = memchr(haystack, *(const unsigned char *)needle, haystacklen)) &&
	        // don't read from past haystack
	        (haystacklen -= (cand-(const unsigned char *)haystack)) >= needlelen
	      ) {
		if (! memcmp(cand, needle, needlelen))
			return (void *)cand;
		--haystacklen;
		haystack = cand + 1;
	}

	return NULL;
}
#endif

#ifndef _GNU_SOURCE
void *mempcpy(void *restrict dest, const void *restrict src, size_t n) {
	return (unsigned char *)memcpy(dest, src, n) + n;
}
#endif



// accepts the buggy strerror_r() from glibc < 2.13
#ifndef _GNU_SOURCE
void error(int status, int errnum, const char *format, ...) {
	// bump it before exit(), so that the incremented value is available in exit
	// callbacks
	++error_message_count;
	fflush(stdout);

	flockfile(stderr);
	if (error_print_progname)
		error_print_progname();
	else fprintf(stderr, "%s: ", program_invocation_name);
	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);  // incl. 'format' = NULL
	va_end(ap);
	if (errnum) {
		char buf[1024];
		if (! strerror_r(errnum, buf, sizeof(buf)))
			fprintf(stderr, ": %s\n", buf);
		else fprintf(stderr, ": Unknown error %d\n", errnum);
	}
	else putc('\n', stderr);
	funlockfile(stderr);

	if (status)
		exit(status);
}
#endif



// CMU SEI CERT: this isn't 100% certain, but in C99 it's the best option.
// Call before free() as well as realloc()!
#ifndef _GNU_SOURCE
void explicit_bzero(void *s, size_t n) {
	volatile unsigned char *ucs = s;
	for (; ucs<(const unsigned char *)s+n; ++ucs)
		*ucs = 0;
}
#endif



#ifndef _GNU_SOURCE
static void twalkr_callback(const void *nodep, VISIT which, int depth) {
	(void)depth;
	const twalkr_data_t *const data = pthread_getspecific(thread_local_twalkr_data);
	data->action(nodep, which, data->closure);
}
#endif

// if root!=NULL, action must be !=NULL
#ifndef _GNU_SOURCE
void twalk_r(const void *root, void (*action)(const void *nodep, VISIT which, void *closure), void *closure) {
	const twalkr_data_t data = {.action = action, .closure = closure};
	if (pthread_setspecific(thread_local_twalkr_data, &data))
		abort();
	twalk(root, twalkr_callback);
}
#endif

#ifndef _GNU_SOURCE
static void tdestroy_callback(const void *node, VISIT pass, void *ctr) {
	(void)node;
	if (pass == preorder || pass == leaf)
		++*(size_t *)ctr;
}
#endif

#ifndef _GNU_SOURCE
static int tdestroy_equal(const void *n1, const void *n2) {
	(void)n1; (void)n2;
	return 0;
}
#endif

// if root!=NULL, free_node must be !=NULL
#ifndef _GNU_SOURCE
void tdestroy(void *root, void (*free_node)(void *nodep)) {
	// could just repeatedly tdelete() the root straight away, but there's no
	// way to tell when the node deleted was the last one (not in POSIX 2004)
	size_t nr = 0;
	twalk_r(root, tdestroy_callback, &nr);
	for (; nr; --nr) {
		free_node(*(void **)root);
		tdelete(root, &root, tdestroy_equal);
	}
}
#endif
