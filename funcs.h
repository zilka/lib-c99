/* Copyright 2023 Roman Žilka

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */



#ifndef FUNCS_H_INCLUDED
#define FUNCS_H_INCLUDED

#include <stdint.h>
#include <limits.h>
#include <sys/types.h>
#include <stdio.h>

#define ARRAY_CHAR
#define ARRAY_PTR
#include "array.h"
#include "array.h"

#define CAT(a, b) a##b
#define CAT_EXP(a, b) CAT(a, b)
#define STRINGIZE(s) #s
#define STRINGIZE_EXP(macro) STRINGIZE(macro)

// Elementary I/O unit in B. Must be >0, fit in size_t, ssize_t, int, off_t as
// well as ptrdiff_t while we're at it.
// BUFSIZ is 2x the I/O size
#ifndef BUFSIZ
#error "BUFSIZ missing, stdio.h not included!"
#endif
#if (BUFSIZ >= 1024) && (BUFSIZ < 65536)
#define IOSIZE ((size_t)BUFSIZ / 2)
#else
#define IOSIZE ((size_t)4096)
#endif

// time_t may be signed, only allow the signed max
#define TIME_MAX ((time_t)(((uintmax_t)1 << (sizeof(time_t)*8-1)) - 1))
// probably in compile-time
#define LE (((const uchar *)&endianity_check_dummy_variable)[0])
#define BE (! ((const uchar *)&endianity_check_dummy_variable)[0])

// printf("%.*s", cutint(len_as_sizet), s));
#define cutint(nonneg_value) ((int)((nonneg_value) & INT_MAX))
#define fitzutu(a) ((size_t)(a) <= PTRDIFF_MAX || ! (errno=EOVERFLOW))
#define fit2zuzu(a, b) ( SIZE_MAX - (size_t)(a) >= (size_t)(b) || \
                         ! (errno = EOVERFLOW) \
                       )
#define eintr_true(condition) do {} while ((condition) && errno == EINTR)
#define eintr_false(condition) do {} while (! (condition) && errno == EINTR)
#define eintr_neg(condition) do {} while ((condition) < 0 && errno == EINTR)

typedef unsigned char uchar;
typedef signed char schar;
typedef int_fast8_t sf8;
typedef int_fast16_t sf16;
typedef uint_fast8_t uf8;
typedef uint_fast16_t uf16;

#include "md5.h"

extern const uf16 endianity_check_dummy_variable;



// TODO: inline string.h func wrappers to take care of NULL ptrs
void *ensalloc(void *restrict p[const restrict static 1], size_t size[const restrict static 1], size_t ensure);
void *growalloc(void *restrict p[const restrict static 1], size_t size[const restrict static 1], size_t n);
ssize_t readlink_full(const char path[restrict static 1], off_t st_size, char *restrict dest[const restrict static 1], size_t destsize[const restrict static 1]);
char *getcwd_full(char *restrict dest[const restrict static 1], size_t destsize[const restrict static 1]);
size_t path_trimsl(char s[const static 1], bool trailing);
bool isdotdir(const char name[static 1]);
const char *hasdotdir(const char path[static 1]);
const char *hasdotdirn(const char path[static 1], size_t len);
char *canonicalize_dirname(const char *path);
char *strexndup(size_t len, const char s[static 1]);
char *strncpy2(char dest[const restrict static 1], const char src[restrict static 1], size_t n);
size_t iosize(int fd, blksize_t blksize);
md5_arith_t *md5(const char path[restrict static 1], blksize_t blksize, md5_arith_t out[const static 4]);
char *err2str(int err, char buf[const static 1], size_t bufsize);
size_t base64enc(size_t srcsize, const uchar *restrict src, char *const restrict dest);
void stablesort(void *const base, size_t nritems, size_t itemsize, int (*cmp)(const void *, const void *));
void *bsearch2(const void *key, const void *base, size_t nel, size_t width, int (*compar)(const void *, const void *));
uf8 u8charsize(const uchar s[static 1], size_t size);
uintmax_t ubitwidth(uintmax_t n);
bool fit2zutu(size_t a, size_t b);
bool fitprod2zuzu(size_t a, size_t b);
bool fitprod2zutu(size_t a, size_t b);

#endif
